module JobtechTaxonomyTest.EuresEsco.Country

open System
open Xunit
open Swensen.Unquote

open System.Collections.Generic

open JobtechTaxonomy2EuresEsco

let countrySamples : obj[] list =
    [
        [|"kTtK_3Y3_Kha"; "Afghanistan"; Some "AFG"; Some "AF" |]
        [|"kWK9_dqr_Mo7"; "Anguilla"; Some "AIA"; Some "AI" |]       
    ]

[<Theory>]
[<MemberData(nameof(countrySamples))>]
let ``Verify multiple examples of GetCountry`` 
        (conceptId:string)
        (preferredLabel: string) 
        (iso_3166_1_alpha_3_2013: string option) 
        (iso_3166_1_alpha_2_2013: string option) =
    test <@
        let concept, country = JobtechTaxonomy2EuresEscoConverter.GetCountry(conceptId)
        let codes = country.Iso_3166_Codes |> Seq.map (|KeyValue|) |> Map.ofSeq |> Map.values |> Set.ofSeq
        let expected = List.choose id [iso_3166_1_alpha_3_2013 ; iso_3166_1_alpha_2_2013] |> Set.ofList
        concept.Id = conceptId
        && country.Id = conceptId
        && country.PreferredLabel = preferredLabel
        &&  codes = expected
        @>

[<Fact>]
let ``Test skill absence in metadata`` () =
    raisesWith<ArgumentException>
        <@ JobtechTaxonomy2EuresEscoConverter.GetCountry("Banana") @>
        (fun e -> <@ e.Message = "No Concept found for Banana" @>)