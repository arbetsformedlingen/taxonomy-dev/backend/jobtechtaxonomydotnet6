module JobtechTaxonomyTest.EuresEsco.Skill

open System
open Xunit
open Swensen.Unquote

open System.Collections.Generic

open JobtechTaxonomy2EuresEsco

[<Fact>]
let ``Test skill presence in metadata`` () =
    let escoIdExpected = Set.ofList ["http://data.europa.eu/esco/skill/0ad6d5e8-593f-4191-843c-439b23a468ca"
                                     "http://data.europa.eu/esco/skill/96d901a6-c931-4e8d-b685-a47e1deaaa1b"]
    let conceptId = "dpBU_3E1_JAx"
    test <@
        let concept, escoIds = JobtechTaxonomy2EuresEscoConverter.GetEscoIdForSkill(conceptId)
        let escoURIs = Seq.map (fun (e:EscoEntry) -> e.EscoURI) escoIds |> Set.ofSeq
        concept.Id = conceptId
        && Seq.length escoIds = 2
        && escoIdExpected = escoURIs
        @>

let skillSamples : obj[] list =
    [
        [|"LXX8_r6E_qWJ"; [ "http://data.europa.eu/esco/skill/51436dad-ed32-4392-8938-bdb9fdb3e188";
                            "http://data.europa.eu/esco/skill/e1a1a201-89ab-402d-815d-af732354e790";
                            "http://data.europa.eu/esco/skill/14084bb9-09b2-42f0-ae6b-53567e7f3e75";]|]
    ]

[<Theory>]
[<MemberData(nameof(skillSamples))>]
let ``Verify multiple examples of GetEscoIdForSkill`` (conceptId:string) (escoIdURIs: string list) =
    test <@
        let concept, escoIds = JobtechTaxonomy2EuresEscoConverter.GetEscoIdForSkill(conceptId)
        concept.Id = conceptId
        && Seq.length escoIds = Seq.length escoIdURIs 
        @>

[<Theory>]
[<MemberData(nameof(skillSamples))>]
let ``Verify multiple escoURIs from GetEscoIdForSkill`` (conceptId:string) (escoIdURIs: string list) =
    test <@
        let expectedURIs = Set.ofList escoIdURIs
        let concept, escoIds = JobtechTaxonomy2EuresEscoConverter.GetEscoIdForSkill(conceptId)
        let escoURIs = Seq.map (fun (e: EscoEntry) -> e.EscoURI) escoIds |> Set.ofSeq
        concept.Id = conceptId
        && escoURIs = expectedURIs
        @>

[<Fact>]
let ``Test skill absence in metadata`` () =
    raisesWith<ArgumentException>
        <@ JobtechTaxonomy2EuresEscoConverter.GetEscoIdForSkill("Banana") @>
        (fun e -> <@ e.Message = "No Concept found for Banana" @>)
