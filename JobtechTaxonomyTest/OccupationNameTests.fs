module JobtechTaxonomyTest.EuresEsco.OccupationName

open System
open Xunit
open Swensen.Unquote

open System.Collections.Generic

open JobtechTaxonomy2EuresEsco

[<Fact>]
let ``Test occupation-name presence in metadata`` () =
    let escoIdExpected = "http://data.europa.eu/esco/isco/C2164"
    let conceptId = "ghs4_JXU_BYt"
    test <@
        let concept, escoIds = JobtechTaxonomy2EuresEscoConverter.GetEscoIdForOccupationName(conceptId)
        let escoId = Seq.head escoIds
        concept.Id = conceptId
        && Seq.length escoIds = 1
        && concept.Type = ConceptType.OccupationName
        && escoId.EscoURI = escoIdExpected
        && escoId.Type = ConceptType.IscoLevel4
        @>

let conceptSamples : obj[] list =
    [
        [|"vqv8_Tiz_NeL"; ["http://data.europa.eu/esco/occupation/cbde1a3a-406d-4d17-b30b-03981925035d";
                           "http://data.europa.eu/esco/occupation/23a61ff1-c954-4867-b982-a018b535f98b";
                           "http://data.europa.eu/esco/occupation/de74eb4e-c95e-425a-9b3a-7be805ec0321"]|]
        [|"eSXv_uCd_b9M"; ["http://data.europa.eu/esco/isco/C3153"]|]
        [|"yMd7_87H_qf2"; ["http://data.europa.eu/esco/occupation/779d13be-60b4-48f3-a339-fe483be96c7f"]|]
        [|"FZMg_T5f_VwH"; ["http://data.europa.eu/esco/isco/C2146"]|]
        [|"UDVa_DtE_Fpb"; ["http://data.europa.eu/esco/isco/C0310"]|]
        [|"KyUN_dkM_sDB"; ["http://data.europa.eu/esco/occupation/43841fa5-f8ae-43ce-834d-5482ec23ef9e";
                           "http://data.europa.eu/esco/occupation/afee6a28-f654-4ac7-a665-f758616bc689"]|]
        [|"aPbL_TNa_Pph"; ["http://data.europa.eu/esco/isco/C7233"]|]
    ]

[<Theory>]
[<MemberData(nameof(conceptSamples))>]
let ``Verify multiple examples of GetEscoIdForOccupationName`` (conceptId:string) (escoIdURIs: string list) =
    test <@
        let concept, escoIds = JobtechTaxonomy2EuresEscoConverter.GetEscoIdForOccupationName(conceptId)
        concept.Id = conceptId
        && Seq.length escoIds = Seq.length escoIdURIs 
        @>

[<Theory>]
[<MemberData(nameof(conceptSamples))>]
let ``Verify multiple escoURIs of GetEscoIdForOccupationName`` (conceptId:string) (escoIdURIs: string list) =
    test <@
        let expectedURIs = Set.ofList escoIdURIs
        let concept, escoIds = JobtechTaxonomy2EuresEscoConverter.GetEscoIdForOccupationName(conceptId)
        let escoURIs = Seq.map (fun (e: EscoEntry) -> e.EscoURI) escoIds |> Set.ofSeq
        concept.Id = conceptId
        && escoURIs = expectedURIs
        @>


[<Fact>]
let ``Test occupation-name absence in metadata`` () =
    raisesWith<ArgumentException>
        <@ JobtechTaxonomy2EuresEscoConverter.GetEscoIdForOccupationName("Banana") @>
        (fun e -> <@ e.Message = "No Concept found for Banana" @>)


