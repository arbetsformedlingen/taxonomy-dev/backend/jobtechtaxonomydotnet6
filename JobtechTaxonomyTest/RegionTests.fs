module JobtechTaxonomyTest.EuresEsco.Region

open System
open Xunit
open Swensen.Unquote

open System.Collections.Generic

open JobtechTaxonomy2EuresEsco

let regionSamples : obj[] list =
    [
        [|"Zuhj_jWY_aJi"; "Region Hannover"; "G3M7_959_8Pp"; Some "DE929"; None |]
        [|"oDpK_oZ2_WYt"; "Dalarnas län"; "i46j_HmG_v64"; Some "SE312"; Some "20" |]
    ]

[<Theory>]
[<MemberData(nameof(regionSamples))>]
let ``Verify multiple examples of GetRegion`` 
        (conceptId:string)
        (preferredLabel: string) 
        (countryId: string) 
        (nutsLevel3Code2021: string option) 
        (nationalNutsLevel3Code2019: string option) =
    test <@
        let concept, region = JobtechTaxonomy2EuresEscoConverter.GetRegion(conceptId)
        let codes = region.Nuts_Codes |> Seq.map (|KeyValue|) |> Map.ofSeq |> Map.values |> Set.ofSeq
        let expected = List.choose id [nutsLevel3Code2021 ; nationalNutsLevel3Code2019] |> Set.ofList
        concept.Id = conceptId
        && region.Id = conceptId
        && region.PreferredLabel = preferredLabel
        && region.CountryId = countryId
        &&  codes = expected
        @>

[<Fact>]
let ``Test skill absence in metadata`` () =
    raisesWith<ArgumentException>
        <@ JobtechTaxonomy2EuresEscoConverter.GetRegion("Banana") @>
        (fun e -> <@ e.Message = "No Concept found for Banana" @>)
