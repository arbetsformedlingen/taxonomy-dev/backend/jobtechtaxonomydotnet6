import json

def json_file(file):
    with open(file, encoding="utf8") as f:
        return json.load(f)

def remove_duplicate_item_dic(dataList):
    seen_items = []
    for dec in dataList:
        if dec not in seen_items:
            seen_items.append(dec)
    #_print(f"Seen items: {seen_items}")
    return seen_items

def find_duplicate_source_dic(dataList):
    new_dict = []
    for item in dataList:
        #_print(line["source"])
        if item["source"] in new_dict:
            new_dict[item["target"]].append(item["target"])
        else:
            new_dict.append({"source":item["source"], "target":[item["target"]]})
    return new_dict

def add_source_data(data1, data2):
    result = []
    data2 = data2["data"]["concepts"]
    #_print(data2)
    for item in data1:
        for concept in data2:
            #_print(concept)
            if item["source"] == concept["id"]:
                    result.append({"source": item["source"],
                                   "source-data": concept,
                                   "target":item["target"]})
    return result

def add_target_data(data1, data2):
     result = []
     for item in data1:
        for target in data2:
             #_print(target)
             if item["target"] == target["id"]:
                     result.append({"source": item["source"],
                                    "source-data": item["source-data"],
                                    "target":target})
     return result

def find_duplicate(data):
    result = []
    for item in data:
        #_print(item)
        if item["source"] not in result:
            result.append(item["source"])
        else:
            print("source is there")
    return result


def main():
    sun_isced_connection_json = json_file("sun-isced-connection.json")
    sun_isced_connection_json = remove_duplicate_item_dic(sun_isced_connection_json)
    #_find_duplicate_data = find_duplicate(sun_isced_connection_json)
    #_print(find_duplicate_data)

    sun_isced_json = json_file("sun-isced.json")
    isced_targets_json = json_file("isced-targets.json")

    #_sun_isced_unique_source_json = find_duplicate_source_dic(sun_isced_connection_json)
    #_print(sun_isced_unique_source_json)
    
    add_source_data_json = add_source_data(sun_isced_connection_json, sun_isced_json)
    #_print(add_source_data_json)

    add_target_data_json = add_target_data(add_source_data_json, isced_targets_json)
    print(add_target_data_json)
    

    with open('sun-education-to-isced.json', 'w') as file:
         json.dump(add_target_data_json, file, ensure_ascii=False)

    

if __name__ == '__main__':
    main()


