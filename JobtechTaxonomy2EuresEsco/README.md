# Eures ESCO

The `JobtechTaxonomy2EuresEsco` package converts valid concept IDs to Eures ESCO mapping IDs for the taxonomy types `skill` and `occupation-name`. It also provides lookups for `language`, `region` and `country` codes.

All functions throw an exception when given an, for the type, invalid concept ID.

## skill

`var result = JobtechTaxonomy2EuresEscoConverter.GetEscoIdForSkill(conceptId);`

## occupation-name

`var result = JobtechTaxonomy2EuresEscoConverter.GetEscoIdForOccupationName(conceptId);`

## language

`var result = JobtechTaxonomy2EuresEscoConverter.GetLanguage(conceptId);`

## region

`var result = JobtechTaxonomy2EuresEscoConverter.GetRegion(conceptId);`

## country

`var result = JobtechTaxonomy2EuresEscoConverter.GetCountry(conceptId);`
