# ISCED

The `JobtechTaxonomy2Isced` package converts valid concept IDs to ISCED mapping IDs for the taxonomy types `sun-education-level-1`, `sun-education-level-2`, `sun-education-level-3`, `sun-education-field-1`, `sun-education-field-2`, and `sun-education-field-3`.

All functions throw an exception when given an, for the type, invalid concept ID.

## SUN -> ISCED

`var result = JobtechTaxonomy2IscedConverter.GetSunIscedId(conceptId);`
