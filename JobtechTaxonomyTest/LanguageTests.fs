module JobtechTaxonomyTest.EuresEsco.Language

open System
open Xunit
open Swensen.Unquote

open System.Collections.Generic

open JobtechTaxonomy2EuresEsco

let languageSamples : obj[] list =
    [
        [|"69WT_Knh_hLs"; "Gaeliska (Skotsk)"; false; Some "GD"; Some "GLA" |]
        [|"Hk93_QrC_QXX"; "Arameiska"; false; None; Some "ARC" |]
        [|"Jw1X_vyk_SbY"; "Urdu"; false; Some "UR"; Some "URD"|]
    ]

[<Theory>]
[<MemberData(nameof(languageSamples))>]
let ``Verify multiple examples of GetLanguage`` (conceptId:string) (preferredLabel: string) (deprecated: bool) (twoCode: string option) (threeCode: string option) =
    test <@
        let concept, language = JobtechTaxonomy2EuresEscoConverter.GetLanguage(conceptId)
        let codes = language.Iso_639_Codes |> Seq.map (|KeyValue|) |> Map.ofSeq |> Map.values |> Set.ofSeq
        let expected = List.choose id [twoCode ; threeCode] |> Set.ofList
        concept.Id = conceptId
        && language.Id = conceptId
        && language.PreferredLabel = preferredLabel
        && language.Deprecated = deprecated
        &&  codes = expected
        @>

[<Fact>]
let ``Test skill absence in metadata`` () =
    raisesWith<ArgumentException>
        <@ JobtechTaxonomy2EuresEscoConverter.GetLanguage("Banana") @>
        (fun e -> <@ e.Message = "No Concept found for Banana" @>)
