# Jobtech Taxonomy Dotnet

To build a package run `dotnet pack` in the root directory of this project
and upload the package `JobtechTaxonomyToEuresEsco.1.21.2.nupkg` in `JobtechTaxonomyDotnet6/JobtechTaxonomy2EuresEsco/bin/Debug/`to https://nexus.jobtechdev.se/

Run the file [curling.sh](curling.sh) to fetch the latest data from the public taxonomy api. The queries are in the `.graphql` files with the corresponding names and the resulting JSON ends up in the `JobtechTaxonomy2EuresEsco` project folder.
