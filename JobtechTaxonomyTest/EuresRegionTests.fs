module JobtechTaxonomyTest.EuresEsco.EuresRegion

open System
open Xunit
open Swensen.Unquote

open System.Collections.Generic

open JobtechTaxonomy2EuresEsco

let regionSamples : obj[] list =
    [
        [|"Zuhj_jWY_aJi"; "Region Hannover"; "G3M7_959_8Pp"; Some "DE929"; None |]
        [|"oDpK_oZ2_WYt"; "Dalarnas län"; "i46j_HmG_v64"; Some "SE312"; Some "20" |]
        [|"CaRE_1nn_cSU"; "Skåne län"; "i46j_HmG_v64"; Some "SE224"; Some "12" |]
        [|"CifL_Rzy_Mku"; "Stockholms län"; "i46j_HmG_v64"; Some "SE110"; Some "01" |]
        [|"osyQ_vch_Cne"; "Prahova"; "t76b_fb1_WCi"; None; None|]
    ]

[<Theory>]
[<MemberData(nameof(regionSamples))>]
let ``Verify multiple examples of GetRegion`` 
        (conceptId:string)
        (preferredLabel: string) 
        (countryId: string) 
        (nutsLevel3Code2021: string option) 
        (nationalNutsLevel3Code2019: string option) =
    test <@
        let concept, region = JobtechTaxonomy2EuresEscoConverter.GetEuresRegion(conceptId)
        let codes = region.Nuts_Codes |> Seq.map (|KeyValue|) |> Map.ofSeq |> Map.values |> Set.ofSeq
        let expected = List.choose id [nutsLevel3Code2021 ; nationalNutsLevel3Code2019] |> Set.ofList
        concept.Id = conceptId
        && region.Id = conceptId
        && region.PreferredLabel = preferredLabel
        && region.CountryId = countryId
        &&  codes = expected
        @>

[<Fact>]
let ``Test skill absence in metadata`` () =
    raisesWith<ArgumentException>
        <@ JobtechTaxonomy2EuresEscoConverter.GetEuresRegion("Banana") @>
        (fun e -> <@ e.Message = "No Concept found for Banana" @>)
