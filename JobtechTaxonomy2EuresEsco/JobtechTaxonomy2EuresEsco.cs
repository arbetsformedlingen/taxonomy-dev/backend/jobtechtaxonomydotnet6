﻿using System.Reflection;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace JobtechTaxonomy2EuresEsco;

[JsonConverter(typeof(StringEnumConverter))]
public enum ConceptType
{
    [EnumMember(Value = "occupation-name")]
    OccupationName,
    [EnumMember(Value = "occupation-field")]
    OccupationField,
    [EnumMember(Value = "esco-occupation")]
    EscoOccupation,
    [EnumMember(Value = "skill")]
    Skill,
    [EnumMember(Value = "skill-headline")]
    SkillHeadline,
    [EnumMember(Value = "esco-skill")]
    EscoSkill,
    [EnumMember(Value = "isco-level-4")]
    IscoLevel4,
    [EnumMember(Value = "ssyk-level-4")]
    SsykLevel4,
    [EnumMember(Value = "language")]
    Language,
    [EnumMember(Value = "continent")]
    Continent,
    [EnumMember(Value = "country")]
    Country
}

public readonly struct EscoEntry
{
    [JsonProperty("id")]
    public readonly string Id;

    [JsonProperty("preferred_label")]
    public readonly string PreferredLabel;

    [JsonProperty("esco_uri")]
    public readonly string EscoURI;

    [JsonProperty("type")]
    public readonly ConceptType Type;

    [JsonProperty("narrower")]
    public readonly TaxonomyEntry Narrower;
}

public enum Iso_639 {
    Iso_639_1_2002,
    Iso_639_2_1998,
    Iso_639_3_2007
}

public readonly struct Language
{
    public Language(TaxonomyEntry taxonomyEntry)
    {
        if (taxonomyEntry.Id.Trim() == "")
        {
            throw new ArgumentException($"No Concept Id found");
        }
        if (taxonomyEntry.PreferredLabel.Trim() == "")
        {
            throw new ArgumentException($"No PreferredLabel for {taxonomyEntry.Id}");
        }
        Dictionary<Iso_639, string> isoDict = new();

        if (taxonomyEntry.Iso_639_1_2002 != null && taxonomyEntry.Iso_639_1_2002.Trim() != "") 
        {
            isoDict.Add(Iso_639.Iso_639_1_2002, taxonomyEntry.Iso_639_1_2002.Trim());
        }
        if (taxonomyEntry.Iso_639_2_1998 != null && taxonomyEntry.Iso_639_2_1998.Trim() != "") 
        {
            isoDict.Add(Iso_639.Iso_639_2_1998, taxonomyEntry.Iso_639_2_1998.Trim());
        }
        if (taxonomyEntry.Iso_639_3_2007 != null && taxonomyEntry.Iso_639_3_2007.Trim() != "") 
        {
            isoDict.Add(Iso_639.Iso_639_3_2007, taxonomyEntry.Iso_639_3_2007.Trim());
        }

        Id = taxonomyEntry.Id;
        PreferredLabel = taxonomyEntry.PreferredLabel;
        Deprecated = taxonomyEntry.Deprecated;
        Iso_639_Codes = isoDict;
    }
    public readonly string Id;
    public readonly string PreferredLabel;
    public readonly bool Deprecated;
    public readonly Dictionary<Iso_639, string> Iso_639_Codes;
}

public enum Nuts {
    NutsLevel3Code2021,
    NationalNutsLevel3Code2019
}

public readonly struct Region
{
    public Region(TaxonomyEntry taxonomyEntry)
    {
        if (taxonomyEntry.Id.Trim() == "")
        {
            throw new ArgumentException($"No Concept Id found");
        }
        if (taxonomyEntry.PreferredLabel.Trim() == "")
        {
            throw new ArgumentException($"No PreferredLabel for {taxonomyEntry.Id}");
        }
        Dictionary<Nuts, string> nutsDict = new();

        if (taxonomyEntry.NationalNutsLevel3Code2019 != null && taxonomyEntry.NationalNutsLevel3Code2019.Trim() != "") 
        {
            nutsDict.Add(Nuts.NationalNutsLevel3Code2019, taxonomyEntry.NationalNutsLevel3Code2019.Trim());
        }
        if (taxonomyEntry.NutsLevel3Code2021 != null && taxonomyEntry.NutsLevel3Code2021.Trim() != "") 
        {
            nutsDict.Add(Nuts.NutsLevel3Code2021, taxonomyEntry.NutsLevel3Code2021.Trim());
        }

        Id = taxonomyEntry.Id;
        PreferredLabel = taxonomyEntry.PreferredLabel;
        Deprecated = taxonomyEntry.Deprecated;
        Nuts_Codes = nutsDict;

        if (taxonomyEntry.Broader.Count > 0)
        {
            var country = taxonomyEntry.Broader.First();
            CountryId = country.Id;
        }
        else
        {
            CountryId = "N/A";
        }
    }
    public readonly string Id;
    public readonly string PreferredLabel;
    public readonly bool Deprecated;
    public readonly string CountryId;
    public readonly Dictionary<Nuts, string> Nuts_Codes;
}

public enum Iso_3166 {
    Iso_3166_1_alpha_3_2013,
    Iso_3166_1_alpha_2_2013
}

public readonly struct Country
{
    public Country(TaxonomyEntry taxonomyEntry)
    {
        if (taxonomyEntry.Id.Trim() == "")
        {
            throw new ArgumentException($"No Concept Id found");
        }
        if (taxonomyEntry.PreferredLabel.Trim() == "")
        {
            throw new ArgumentException($"No PreferredLabel for {taxonomyEntry.Id}");
        }
        Dictionary<Iso_3166, string> isoDict = new();

        if (taxonomyEntry.Iso_3166_1_alpha_2_2013 != null && taxonomyEntry.Iso_3166_1_alpha_2_2013.Trim() != "") 
        {
            isoDict.Add(Iso_3166.Iso_3166_1_alpha_2_2013, taxonomyEntry.Iso_3166_1_alpha_2_2013.Trim());
        }
        if (taxonomyEntry.Iso_3166_1_alpha_3_2013 != null && taxonomyEntry.Iso_3166_1_alpha_3_2013.Trim() != "") 
        {
            isoDict.Add(Iso_3166.Iso_3166_1_alpha_3_2013, taxonomyEntry.Iso_3166_1_alpha_3_2013.Trim());
        }

        Id = taxonomyEntry.Id;
        PreferredLabel = taxonomyEntry.PreferredLabel;
        Deprecated = taxonomyEntry.Deprecated;
        Iso_3166_Codes = isoDict;
    }
    public readonly string Id;
    public readonly string PreferredLabel;
    public readonly bool Deprecated;
    public readonly Dictionary<Iso_3166, string> Iso_3166_Codes;
}

public readonly struct TaxonomyEntry
{
    [JsonProperty("id")]
    public readonly string Id;

    [JsonProperty("deprecated")]
    public readonly bool Deprecated;

    [JsonProperty("preferred_label")]
    public readonly string PreferredLabel;

    #region iso language
    [JsonProperty("iso_639_1_2002")]
    public readonly string Iso_639_1_2002;

    [JsonProperty("iso_639_2_1998")]
    public readonly string Iso_639_2_1998;

    [JsonProperty("iso_639_3_2007")]
    public readonly string Iso_639_3_2007;

    #endregion

    #region nuts region
    [JsonProperty("nuts_level_3_code_2021")]
    public readonly string NutsLevel3Code2021;

    [JsonProperty("national_nuts_level_3_code_2019")]
    public readonly string NationalNutsLevel3Code2019;
    #endregion

    #region iso country
    [JsonProperty("iso_3166_1_alpha_3_2013")]
    public readonly string Iso_3166_1_alpha_3_2013;

    [JsonProperty("iso_3166_1_alpha_2_2013")]
    public readonly string Iso_3166_1_alpha_2_2013;
    #endregion

    [JsonProperty("type")]
    public readonly ConceptType Type;

    [JsonProperty("no_esco_relation")]
    public readonly bool NoEscoRelation;

    [JsonProperty("exact_match", DefaultValueHandling = DefaultValueHandling.Populate)]
    public readonly List<EscoEntry> ExactMatches;

    [JsonProperty("broad_match", DefaultValueHandling = DefaultValueHandling.Populate)]
    public readonly List<EscoEntry> BroadMatches;

    [JsonProperty("broader", DefaultValueHandling = DefaultValueHandling.Populate)]
    public readonly List<EscoEntry> Broader;

    [JsonProperty("narrower", DefaultValueHandling = DefaultValueHandling.Populate)]
    public readonly List<TaxonomyEntry> Narrower;

    [JsonProperty("narrow_match", DefaultValueHandling = DefaultValueHandling.Populate)]
    public readonly List<EscoEntry> NarrowMatches;

    [JsonProperty("close_match", DefaultValueHandling = DefaultValueHandling.Populate)]
    public readonly List<EscoEntry> CloseMatches;
}

public readonly struct TaxonomyConcepts
{
    [JsonProperty("concepts")]
    public readonly List<TaxonomyEntry> Concepts;

    public TaxonomyConcepts(List<TaxonomyEntry> concepts)
    {
        Concepts = concepts;
    }
}

public readonly struct TaxonomyData
{
    [JsonProperty("data")]
    public readonly TaxonomyConcepts Data;

    public TaxonomyData(TaxonomyConcepts data)
    {
        Data = data;
    }
}

public class JobtechTaxonomy2EuresEscoConverter
{
    static readonly TaxonomyData taxonomySkills;
    static readonly TaxonomyData taxonomyOccupationNames;
    static readonly TaxonomyData taxonomyLanguages;
    static readonly TaxonomyData taxonomyRegions;
    static readonly TaxonomyData taxonomyCountries;
    static readonly TaxonomyData taxonomyEuresRegions;

    static TaxonomyData LoadTaxonomy(string resourceName)
    {
        var assembly = typeof(JobtechTaxonomy2EuresEsco.JobtechTaxonomy2EuresEscoConverter).GetTypeInfo().Assembly;
        var resource = assembly.GetManifestResourceStream(resourceName);
        if (resource != null)
        {
            return JsonConvert.DeserializeObject<TaxonomyData>(new StreamReader(resource).ReadToEnd());
        }

        throw new ArgumentNullException(resourceName);
    }

    static TaxonomyData LoadNestedTaxonomy(string resourceName)
    {
        var assembly = typeof(JobtechTaxonomy2EuresEsco.JobtechTaxonomy2EuresEscoConverter).GetTypeInfo().Assembly;
        var resource = assembly.GetManifestResourceStream(resourceName);
        if (resource != null)
        {
            var nestedConcepts = JsonConvert.DeserializeObject<TaxonomyData>(new StreamReader(resource).ReadToEnd());
            var taxonomyEntry = new List <TaxonomyEntry>();
            foreach (TaxonomyEntry continent in nestedConcepts.Data.Concepts)
            {
                foreach (TaxonomyEntry country in continent.Narrower)
                {
                    foreach (TaxonomyEntry region in country.Narrower)
                    {
                        taxonomyEntry.Add(region);
                    }
                }
            }
            var concepts = new TaxonomyConcepts(taxonomyEntry);
            var newTaxonomyData = new TaxonomyData(concepts);
            return newTaxonomyData;
        }

        throw new ArgumentNullException(resourceName);
    }

    static JobtechTaxonomy2EuresEscoConverter()
    {
        taxonomySkills = LoadTaxonomy("JobtechTaxonomy2EuresEsco.CuratedFiles.skill-to-esco.json");
        taxonomyOccupationNames = LoadTaxonomy("JobtechTaxonomy2EuresEsco.CuratedFiles.occupation-name-to-esco.json");
        taxonomyLanguages = LoadTaxonomy("JobtechTaxonomy2EuresEsco.language.json");
        taxonomyRegions = LoadTaxonomy("JobtechTaxonomy2EuresEsco.region.json");
        taxonomyCountries = LoadTaxonomy("JobtechTaxonomy2EuresEsco.country.json");
        taxonomyEuresRegions = LoadNestedTaxonomy("JobtechTaxonomy2EuresEsco.eures-region.json");
    }

    public static List<EscoEntry> GetOccupationNameEscoId(TaxonomyEntry taxonomyEntry)
    {
        var exact_match = taxonomyEntry.ExactMatches.Where(e => e.EscoURI != null).ToList();
        var broad_match = taxonomyEntry.BroadMatches.Where(e => e.EscoURI != null).ToList();
        var narrow_match = taxonomyEntry.NarrowMatches.Where(e => e.EscoURI != null).ToList();
        var close_match = taxonomyEntry.CloseMatches.Where(e => e.EscoURI != null).ToList();
        var broader = taxonomyEntry.Broader.Where(e => e.EscoURI != null).ToList();

        var all = new List<EscoEntry>();
        all.AddRange(exact_match);
        all.AddRange(broad_match);
        all.AddRange(narrow_match);
        all.AddRange(close_match);

        // DOes NOC have multiple mappings to ESCO?
        if (all.Count > 1)
        {
            // Do the matching mappings include 3 or less exact ones?
            if (exact_match.Count > 0 && exact_match.Count <= 3)
            {
                // Send the exact ESCO mappings (max 3).
                return exact_match;
            }

            // Are there more than 3 mappings?
            if (all.Count > 3)
            {
                return broader;
            }

            // Are they more specific than the NOC?
            if (narrow_match.Count > 0)
            {
                return broader;
            }
        }

        return all;
    }

    public static List<EscoEntry> GetSkillEscoId(TaxonomyEntry taxonomyEntry)
    {
        var all = new List<EscoEntry>();
        all.AddRange(taxonomyEntry.ExactMatches.Where(e => e.EscoURI != null));
        all.AddRange(taxonomyEntry.NarrowMatches.Where(e => e.EscoURI != null));
        all.AddRange(taxonomyEntry.CloseMatches.Where(e => e.EscoURI != null));
        all.AddRange(taxonomyEntry.BroadMatches.Where(e => e.EscoURI != null));
        return all;
    }

    public static TaxonomyEntry GetTaxonomyEntry(TaxonomyData taxonomyData, string conceptId)
    {
        var concept = taxonomyData.Data.Concepts.Find(x => x.Id == conceptId);
        if (concept.Id == conceptId)
        {
            return concept;
        }
        throw new ArgumentException($"No Concept found for {conceptId}");
    }

    public static Tuple<TaxonomyEntry, Language> GetLanguage(string conceptId)
    {
        var concept = GetTaxonomyEntry(taxonomyLanguages, conceptId);
        var language = new Language(concept);
        return new Tuple<TaxonomyEntry, Language>(concept, language);
    }

    public static Tuple<TaxonomyEntry, List<EscoEntry>> GetEscoIdForSkill(string conceptId)
    {
        var concept = GetTaxonomyEntry(taxonomySkills, conceptId);
        var mapping = GetSkillEscoId(concept);
        return new Tuple<TaxonomyEntry, List<EscoEntry>>(concept, mapping);
    }

    public static Tuple<TaxonomyEntry, List<EscoEntry>> GetEscoIdForOccupationName(string conceptId)
    {
        var concept = GetTaxonomyEntry(taxonomyOccupationNames, conceptId);
        var mapping = GetOccupationNameEscoId(concept);
        return new Tuple<TaxonomyEntry, List<EscoEntry>>(concept, mapping);
    }

    public static Tuple<TaxonomyEntry, Region> GetRegion(string conceptId)
    {
        var concept = GetTaxonomyEntry(taxonomyRegions, conceptId);
        var region = new Region(concept);
        return new Tuple<TaxonomyEntry, Region>(concept, region);
    }

    public static Tuple<TaxonomyEntry, Country> GetCountry(string conceptId)
    {
        var concept = GetTaxonomyEntry(taxonomyCountries, conceptId);
        var country = new Country(concept);
        return new Tuple<TaxonomyEntry, Country>(concept, country);
    }

    public static Tuple<TaxonomyEntry, Region> GetEuresRegion(string conceptId)
    {
        var concept = GetTaxonomyEntry(taxonomyEuresRegions, conceptId);
        var euresregion = new Region(concept);
        return new Tuple<TaxonomyEntry, Region>(concept, euresregion);
    }

}
