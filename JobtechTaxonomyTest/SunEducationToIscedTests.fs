module JobtechTaxonomyTest.Isced

open System
open Xunit
open Swensen.Unquote

open System.Collections.Generic

open JobtechTaxonomy2Isced

[<Fact>]
let ``Test Isced Field code from sun concept`` () =
    let conceptId = "y1xq_3kZ_e34"
    test <@
        let concept, isced = JobtechTaxonomy2IscedConverter.GetSunIscedField(conceptId)
        concept.Id = conceptId
        && concept.Type = "sun-education-field-1"
        && concept.PreferredLabel = "Pedagogik och lärarutbildning"
        && concept.Definition = "Pedagogik och lärarutbildning" 
        && concept.SunEducationFieldCode2000 = "1" 
        && isced.Id = "01"
        && isced.PreferredLabel = "Education"
        && isced.IscedType = "isced-field" 
        @>

[<Fact>]
let ``Test Isced Level code from sun concept`` () =
    let conceptId = "asZm_TEQ_Srk"
    test <@
        let concept, isced = JobtechTaxonomy2IscedConverter.GetSunIscedLevel(conceptId)
        concept.Id = conceptId
        && concept.Type = "sun-education-level-1"
        && concept.PreferredLabel = "Eftergymnasial utbildning två år eller längre"
        && concept.Definition = "Eftergymnasial utbildning två år eller längre" 
        && isced.Id = "5"
        && isced.PreferredLabel =  "Short-cycle tertiary education"
        && isced.IscedType = "isced-level"
        @>


[<Fact>]
let ``Test concept absence in level metadata`` () =
    raisesWith<ArgumentException>
        <@ JobtechTaxonomy2IscedConverter.GetSunIscedLevel("Banana") @>
        (fun e -> <@ e.Message = "No Concept found for Banana" @>)


[<Fact>]
let ``Test concept absence in field metadata`` () =
    raisesWith<ArgumentException>
        <@ JobtechTaxonomy2IscedConverter.GetSunIscedField("Banana") @>
        (fun e -> <@ e.Message = "No Concept found for Banana" @>)



[<Fact>]
let ``Test field concept absence in level metadata`` () =
    raisesWith<ArgumentException>
        <@ JobtechTaxonomy2IscedConverter.GetSunIscedLevel("y1xq_3kZ_e34") @>
        (fun e -> <@ e.Message = "No Concept found for y1xq_3kZ_e34" @>)


[<Fact>]
let ``Test level concept absence in field metadata`` () =
    raisesWith<ArgumentException>
        <@ JobtechTaxonomy2IscedConverter.GetSunIscedField("asZm_TEQ_Srk") @>
        (fun e -> <@ e.Message = "No Concept found for asZm_TEQ_Srk" @>)

