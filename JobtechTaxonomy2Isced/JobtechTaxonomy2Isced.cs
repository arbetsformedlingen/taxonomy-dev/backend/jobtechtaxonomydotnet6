﻿using System.Reflection;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace JobtechTaxonomy2Isced;

public readonly struct IscedEntry
{
    [JsonProperty("id")]
    public readonly string Id;

    [JsonProperty("preferred_label")]
    public readonly string PreferredLabel;

    [JsonProperty("type")]
    public readonly string IscedType;
}

public readonly struct TaxonomyEntry
{
    [JsonProperty("id")]
    public readonly string Id;

    [JsonProperty("type")]
    public readonly string Type;

    [JsonProperty("preferred_label")]
    public readonly string PreferredLabel;

    [JsonProperty("definition")]
    public readonly string Definition;

    [JsonProperty("sun_education_field_code_2000")]
    public readonly string SunEducationFieldCode2000;
}

public readonly struct SunEducationToIsced
{
    [JsonProperty("source")]
    public readonly string Source;

    [JsonProperty("source-data")]
    public readonly TaxonomyEntry SourceData;

    [JsonProperty("target")]
    public readonly IscedEntry Target;
}

public readonly struct SunToIscedEntries
{
    [JsonProperty("edges")]
    public readonly List<SunEducationToIsced> Edges;
}

public class JobtechTaxonomy2IscedConverter
{
    static readonly Dictionary<string, Tuple<TaxonomyEntry, IscedEntry>> sunToIscedConverter;

    static SunToIscedEntries LoadJsonData(string resourceName)
    {
        var assembly = typeof(JobtechTaxonomy2Isced.JobtechTaxonomy2IscedConverter).GetTypeInfo().Assembly;
        var resource = assembly.GetManifestResourceStream(resourceName);
        if (resource != null)
        {
            return JsonConvert.DeserializeObject<SunToIscedEntries>(new StreamReader(resource).ReadToEnd());
        }

        throw new ArgumentNullException(resourceName);
    }

    static JobtechTaxonomy2IscedConverter()
    {
        var iscedEntries = LoadJsonData("JobtechTaxonomy2Isced.sun-education-to-isced.json");
        sunToIscedConverter = new();
        foreach (var entry in iscedEntries.Edges)
        {
            sunToIscedConverter.Add(entry.Source,
                new Tuple<TaxonomyEntry, IscedEntry>(entry.SourceData, entry.Target));
        }
    }

    public static Tuple<TaxonomyEntry, IscedEntry> GetSunIscedLevel(string conceptId)
    {
        if (sunToIscedConverter.TryGetValue(conceptId, out Tuple<TaxonomyEntry, IscedEntry>? response))
        {
            if (response.Item2.IscedType == "isced-level") {
                return response;
            }
        }
        throw new ArgumentException($"No Concept found for {conceptId}");
    }
    public static Tuple<TaxonomyEntry, IscedEntry> GetSunIscedField(string conceptId)
    {
        if (sunToIscedConverter.TryGetValue(conceptId, out Tuple<TaxonomyEntry, IscedEntry>? response))
        {
            if (response.Item2.IscedType == "isced-field") {
                return response;
            }
        }
        throw new ArgumentException($"No Concept found for {conceptId}");
    }
}
