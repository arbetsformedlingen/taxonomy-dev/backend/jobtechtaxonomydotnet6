import json
import re

def json_file(file):
    with open(file, encoding="utf8") as f:
        tax_json = json.load(f)
        return tax_json["data"]["concepts"]
    
def csv_file(file):
    with open(file) as f:
        return f.readlines()
    
def flatten_output(xss):
    return [x for xs in xss for x in xs]

def remove_duplicate(mylist):
    res_list = []
    for i in range(len(mylist)):
        if mylist[i] not in mylist[i + 1:]:
            res_list.append(mylist[i])
    return res_list

def split_number(row):
     if ((row is not None) and (re.search('[a-zA-Z]', row))):
        #print(row)
        row_split = re.split('(\d+)', row)
        #print(row_split[1])
        return row_split[1]

def nyckel_SUN2000Niva_ISCED97(jsonfile, csvfile):
   result = []
   json_f = json_file(jsonfile)
   csv_f = csv_file(csvfile)
   for row_json in json_f:
       level_code_2020 = split_number(row_json["sun_education_level_code_2020"])
       level_code_2000 = split_number(row_json["sun_education_level_code_2000"])
       field_code_2000 = split_number(row_json["sun_education_field_code_2000"])
       field_code_2020 = split_number(row_json["sun_education_field_code_2020"])
       for row_csv in csv_f[1:]:
            col = row_csv.split(";")
            if ((col[0] == row_json["sun_education_level_code_2020"]) or
                (col[0] == row_json["sun_education_level_code_2000"]) or
                (col[0] == row_json["sun_education_field_code_2000"]) or
                (col[0] == row_json["sun_education_field_code_2020"])):
                result.append({"id": row_json["id"], 
                                "preferred_label": row_json["preferred_label"] ,
                                "type": row_json["type"], 
                                "sun_education_level_code_2020": row_json["sun_education_level_code_2020"],
                                "sun_education_level_code_2000": row_json["sun_education_level_code_2000"],
                                "sun_education_field_code_2000": row_json["sun_education_field_code_2000"],
                                "sun_education_field_code_2020": row_json["sun_education_field_code_2020"],
                                "SUN2000Niva": col[0].strip(),
                                "SUN2000NivaText": col[1].strip(),
                                "ISCED97Niva": col[2].strip(),                                                                    
                                })
            elif ((col[0] == level_code_2020) or
                  (col[0] == level_code_2000) or
                  (col[0] == field_code_2000) or
                  (col[0] == field_code_2020)):
                  result.append({"id": row_json["id"], 
                           "preferred_label": row_json["preferred_label"] ,
                            "type": row_json["type"], 
                            "sun_education_level_code_2020": row_json["sun_education_level_code_2020"],
                            "sun_education_level_code_2000": row_json["sun_education_level_code_2000"],
                            "sun_education_field_code_2000": row_json["sun_education_field_code_2000"],
                            "sun_education_field_code_2020": row_json["sun_education_field_code_2020"],
                            "SUN2000Niva": col[0].strip(),
                            "SUN2000NivaText": col[1].strip(),
                            "ISCED97Niva": col[2].strip(),                                                                    
                            })
   
   return result

def nyckel_SUN2020Niva_ISCED2011_1(jsonfile, csvfile):
   result = []
   json_f = json_file(jsonfile)
   csv_f = csv_file(csvfile)
   for row_json in json_f:
       level_code_2020 = split_number(row_json["sun_education_level_code_2020"])
       level_code_2000 = split_number(row_json["sun_education_level_code_2000"])
       field_code_2000 = split_number(row_json["sun_education_field_code_2000"])
       field_code_2020 = split_number(row_json["sun_education_field_code_2020"])
       for row_csv in csv_f[1:]:
            col = row_csv.split(";")
            if ((col[0] == row_json["sun_education_level_code_2020"]) or
                (col[0] == row_json["sun_education_level_code_2000"]) or
                (col[0] == row_json["sun_education_field_code_2000"]) or
                (col[0] == row_json["sun_education_field_code_2020"])):
                result.append({"id": row_json["id"], 
                            "preferred_label": row_json["preferred_label"] ,
                                "type": row_json["type"], 
                                "sun_education_level_code_2020": row_json["sun_education_level_code_2020"],
                                "sun_education_level_code_2000": row_json["sun_education_level_code_2000"],
                                "sun_education_field_code_2000": row_json["sun_education_field_code_2000"],
                                "sun_education_field_code_2020": row_json["sun_education_field_code_2020"],
                                "SUN2020Niva": col[0].strip(),
                                "Benämning": col[1].strip(),
                                "ISCED2011Niva_1": col[2].strip(),  
                                "ISCED2011Niva_1_Text_Sv": col[3].strip(), 
                                "ISCED2011Niva_1_Text_ENG": col[4].strip(),                                                                    
                                })
            elif ((col[0] == level_code_2020) or
                  (col[0] == level_code_2000) or
                  (col[0] == field_code_2000) or
                  (col[0] == field_code_2020)):
                  result.append({"id": row_json["id"], 
                            "preferred_label": row_json["preferred_label"] ,
                                "type": row_json["type"], 
                                "sun_education_level_code_2020": row_json["sun_education_level_code_2020"],
                                "sun_education_level_code_2000": row_json["sun_education_level_code_2000"],
                                "sun_education_field_code_2000": row_json["sun_education_field_code_2000"],
                                "sun_education_field_code_2020": row_json["sun_education_field_code_2020"],
                                "SUN2020Niva": col[0].strip(),
                                "Benämning": col[1].strip(),
                                "ISCED2011Niva_1": col[2].strip(),  
                                "ISCED2011Niva_1_Text_Sv": col[3].strip(), 
                                "ISCED2011Niva_1_Text_ENG": col[4].strip(),                                                                
                                })
   
   return result

def common_SUN2000_SUN2020(jsonfile, csvfile, key0, key1, key2, key3):
   result = []
   json_f = json_file(jsonfile)
   csv_f = csv_file(csvfile)
   for row_json in json_f:
       level_code_2020 = split_number(row_json["sun_education_level_code_2020"])
       level_code_2000 = split_number(row_json["sun_education_level_code_2000"])
       field_code_2000 = split_number(row_json["sun_education_field_code_2000"])
       field_code_2020 = split_number(row_json["sun_education_field_code_2020"])
       for row_csv in csv_f[1:]:
            col = row_csv.split(";")
            if ((col[0] == row_json["sun_education_level_code_2020"]) or
                (col[0] == row_json["sun_education_level_code_2000"]) or
                (col[0] == row_json["sun_education_field_code_2000"]) or
                (col[0] == row_json["sun_education_field_code_2020"])):
                result.append({"id": row_json["id"], 
                            "preferred_label": row_json["preferred_label"] ,
                                "type": row_json["type"], 
                                "sun_education_level_code_2020": row_json["sun_education_level_code_2020"],
                                "sun_education_level_code_2000": row_json["sun_education_level_code_2000"],
                                "sun_education_field_code_2000": row_json["sun_education_field_code_2000"],
                                "sun_education_field_code_2020": row_json["sun_education_field_code_2020"],
                                key0: col[0].strip(),
                                key1: col[1].strip(),
                                key2: col[2].strip(),  
                                key3: col[3].strip(),                                                                    
                                })
            elif ((col[0] == level_code_2020) or
                  (col[0] == level_code_2000) or
                  (col[0] == field_code_2000) or
                  (col[0] == field_code_2020)):
                   result.append({"id": row_json["id"], 
                            "preferred_label": row_json["preferred_label"] ,
                                "type": row_json["type"], 
                                "sun_education_level_code_2020": row_json["sun_education_level_code_2020"],
                                "sun_education_level_code_2000": row_json["sun_education_level_code_2000"],
                                "sun_education_field_code_2000": row_json["sun_education_field_code_2000"],
                                "sun_education_field_code_2020": row_json["sun_education_field_code_2020"],
                                key0: col[0].strip(),
                                key1: col[1].strip(),
                                key2: col[2].strip(),  
                                key3: col[3].strip(),                                                                
                                })
   
   return result

def main():
   output_list = []
   nyckel_SUN2000Niva = nyckel_SUN2000Niva_ISCED97('sun-education-to-isced.json', 
                                                   '../JobtechTaxonomy2Isced/nyckel-sun-2020_isced2011_isced97_Nyckel_SUN2000Niva_ISCED97.csv')
   nyckel_SUN2020Niva = nyckel_SUN2020Niva_ISCED2011_1('sun-education-to-isced.json', 
                                                       '../JobtechTaxonomy2Isced/nyckel-sun-2020_isced2011_isced97_Nyckel_SUN2020Niva_ISCED2011_1.csv')
   sun2000Inr_3_ISCED = common_SUN2000_SUN2020('sun-education-to-isced.json', 
                                        '../JobtechTaxonomy2Isced/sun-2000-isced-2013-f_SUN2000Inr_3_ISCED_F_2013.csv', 
                                        "SUN2000Inr_3", "SUN2000Inr_3_Text", "ISCED_F_2013", "ISCED_F_2013_Text_Detailed_field")
   sun2000Inr_ISCED = common_SUN2000_SUN2020('sun-education-to-isced.json', 
                                        '../JobtechTaxonomy2Isced/sun-2000-isced-2013-f_SUN2000Inr_ISCED_F_2013.csv', 
                                        "SUN2000Inr", "SUN2000InrText", "ISCED_F_2013", "ISCED_F_2013_Text_Detailed_field")
   sun2020Inr_2_ISCED = common_SUN2000_SUN2020('sun-education-to-isced.json', 
                                        '../JobtechTaxonomy2Isced/sun-2020-isced-2013-f_SUN2020Inr_2_ISCED_F_2013_2.csv', 
                                        "SUN2020Inr_2", "Benämning", "ISCED F-2013_2", "Description")
   sun2020Inr_3_ISCED = common_SUN2000_SUN2020('sun-education-to-isced.json', 
                                        '../JobtechTaxonomy2Isced/sun-2020-isced-2013-f_SUN2020Inr_3_ISCED_F_2013_4.csv', 
                                        "SUN2020Inr_3", "Benämning", "ISCED F-2013_4", "Description")
   sun2020Inr_4_ISCED = common_SUN2000_SUN2020('sun-education-to-isced.json', 
                                        '../JobtechTaxonomy2Isced/sun-2020-isced-2013-f_SUN2020Inr_4_ISCED_F_2013_4.csv', 
                                        "SUN2020Inr_4", "Benämning", "ISCED F-2013_4", "Description")

   output_list.append(nyckel_SUN2000Niva)
   output_list.append(nyckel_SUN2020Niva)
   output_list.append(sun2000Inr_3_ISCED)
   output_list.append(sun2000Inr_ISCED)
   output_list.append(sun2020Inr_2_ISCED)
   output_list.append(sun2020Inr_3_ISCED)
   output_list.append(sun2020Inr_4_ISCED)

   output_list = flatten_output(output_list)
   output_list = remove_duplicate(output_list)
   
   #print(output_list)
   print("output_list len:", len(output_list))

   with open('sun-education-to-isced-expanded{0}.json'.format(0), 'w') as file:
       json.dump(output_list, file, ensure_ascii=False)

if __name__ == '__main__':
    main()

